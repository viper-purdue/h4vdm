include manifest
export

# get site packages location
PYTHONEXE=$(Python3_ROOT_DIR)/python
#PYTHON_PACKAGE_DIR=$(shell $(PYTHONEXE) -c 'import site; print(site.getsitepackages()[0])')
#$(info python package directory is "$(PYTHON_PACKAGE_DIR)")
#export Python3_NumPy_INCLUDE_DIR=$(PYTHON_PACKAGE_DIR)
export protobuf_BUILD_TESTS=OFF

all:
	mkdir -p build
	cd build; cmake -Dprotobuf_BUILD_TESTS=OFF ..; make -j $(NUM_BUILD_THREADS)

clean:
	cd build; make clean