# H264 Encoding Parameters Extraction

This is the H.264 encoding parameters extraction implementation for [H4VDM: H.264 Video Device Matching](https://arxiv.org/abs/2210.11549).

Authors: [Ziyue "Alan" Xiang](https://www.alanshawn.com), Paolo Bestagini, Stefano Tubaro, Edward J. Delp

Please direct all correspondence to Prof. Edward J. Delp (ace@ecn.purdue.edu).


## Building

- OS requirements: Ubuntu 20.04+
- Dependent packages: `gcc`, `nasm`, `cmake`, `ffmpeg`
- Run `make` to build the modified `openh264` codec
- Once building is finished, the executable `bin/h264dec_ext_info` will be available


## Usage

See [example/example.ipynb](./example/example.ipynb) (install packages as per [example/requirements.txt](./example/requirements.txt))


## Similar projects

- [trestles](https://github.com/LLNL/trestles) (JM-baesd encoding parameters extraction)