#pragma once

#if defined (_WIN32)
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <tchar.h>
#else
#include <string.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#if defined (ANDROID_NDK)
#include <android/log.h>
#endif
#include "codec_def.h"
#include "codec_app_def.h"
#include "codec_api.h"
#include "read_config.h"
#include "typedefs.h"
#include "measure_time.h"
#include "d3d9_utils.h"

#include "nlohmann/json.hpp"

using json = nlohmann::json;
using namespace std;

#if defined (WINDOWS_PHONE)
double g_dDecTime = 0.0;
float  g_fDecFPS = 0.0;
int    g_iDecodedFrameNum = 0;
#endif

#if defined(ANDROID_NDK)
#define LOG_TAG "welsdec"
#define LOGI(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define printf LOGI
#define fprintf(a, ...) LOGI(__VA_ARGS__)
#endif

int32_t readBit (uint8_t* pBufPtr, int32_t& curBit);

int32_t readBits (uint8_t* pBufPtr, int32_t& n, int32_t& curBit);

int32_t bsGetUe (uint8_t* pBufPtr, int32_t& curBit);

int32_t readFirstMbInSlice (uint8_t* pSliceNalPtr);

int32_t readPicture (uint8_t* pBuf, const int32_t& iFileSize, const int32_t& bufPos, uint8_t*& pSpsBuf,
                     int32_t& sps_byte_count);

void FlushFrames (ISVCDecoder* pDecoder, int64_t& iTotal, FILE* pYuvFile, json* pOption, int32_t& iFrameCount,
                  unsigned long long& uiTimeStamp, int32_t& iWidth, int32_t& iHeight, int32_t& iLastWidth, int32_t iLastHeight);

void H264DecodeInstance (ISVCDecoder* pDecoder, const char* kpH264FileName, const char* kpOuputFileName,
                         int32_t& iWidth, int32_t& iHeight, json* pOption, const char* pLengthFileName,
                         int32_t iErrorConMethod,
                         bool bLegacyCalling);


