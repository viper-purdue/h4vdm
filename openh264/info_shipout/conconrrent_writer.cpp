#include "conconrrent_writer.h"
#include <netinet/in.h>

namespace viper_purdue{

    int total_frame_count = 0;

    ConcurrentWriter::ConcurrentWriter(const std::string &out_filename) {
        context.outfile.open(out_filename, ios::out | ios::binary);
        // spawn writer thread
        auto t = thread(&file_writer_thread, &context);
        t.detach();
    }

    void ConcurrentWriter::write(const std::string &data) {
        context.queue.enqueue(data);
    }

    ConcurrentWriter::~ConcurrentWriter() noexcept(false){
        if(!context.writer_finished) {
            throw runtime_error{"writer has not exited! call terminate() before exiting."};
        }
        context.outfile.close();
    }

    void ConcurrentWriter::terminate() {
        // wait for a while for everything to enque
        this_thread::sleep_for(chrono::milliseconds(10));
        context.end_writing = true;
        // busy wait for the writer thread to quit
        while(!context.writer_finished) {
            this_thread::sleep_for(chrono::milliseconds(1));
        }
    }

    uint32_t ensure_small_endian_integer(uint32_t num) {
        if ( htonl(47) == 47 ) {
            // Big endian
            return __builtin_bswap32(num);
        } else {
            return num;
        }
    }

    void file_writer_thread(ConcurrentWriterContext *context) {
        string data;
        while (true) {
            auto suceess = context->queue.try_dequeue(data);
            if(suceess) {
                // get data size in small endian
                auto se_size = ensure_small_endian_integer(data.size());
                // write size
                context->outfile.write(reinterpret_cast<char*>(&se_size), sizeof(se_size));
                // write data
                context->outfile.write(data.data(), data.size());
            }else{
                // see if writing has finished
                if(context->end_writing) {
                    context->writer_finished = true;
                    break;
                }
                // if nothing to write, sleep for a short while
                this_thread::sleep_for(chrono::milliseconds(1));
            }
        }
    }

    float mv_int_to_float(int16_t mv) {
        return static_cast<float>(mv) / 4.0;
        auto decimal = (mv & (int16_t) 0x03) * 0.25f;
        auto integer = static_cast<float>(mv >> 2);
        return integer + decimal;
    }
}


