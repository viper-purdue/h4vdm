#pragma once

#include "slice.pb.h"
#include "concurrentqueue.h"
#include <fstream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <string>
#include <atomic>
#include <filesystem>
#include <iostream>
#include <chrono>
#include <thread>

namespace viper_purdue {

    extern int total_frame_count;

    using namespace std;
    using moodycamel::ConcurrentQueue;

    uint32_t ensure_small_endian_integer(uint32_t num);

    struct ConcurrentWriterContext {
        ConcurrentQueue<string> queue;
        atomic<bool> end_writing = false;
        atomic<bool> writer_finished = false;
        fstream outfile;
    };

    float mv_int_to_float(int16_t mv);

    void file_writer_thread(ConcurrentWriterContext *context);

    class ConcurrentWriter {
    public:

        ConcurrentWriter(const string &out_filename);

        ~ConcurrentWriter() noexcept(false);

        void write(const string &data);

        void terminate();

    private:
        ConcurrentWriterContext context;
    };

}
