#include "h264_dec_util.h"
#include "argparse.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <filesystem>
#include <fstream>

using namespace std;


argparse::ArgumentParser parse_args(int argc, char* argv[]){
    argparse::ArgumentParser program("h264dec_extra_info");
    program.add_argument("h264_video");
    program.add_argument("--yuv_out");
    program.add_argument("--info_out");
    program.add_argument("--option_out");
    program.add_argument("--verbosity").default_value(1)
        .action([](const string &val){
            auto n_val = stoi(val);
            return n_val == 0 ? 0 : 1 << n_val;
        });
    program.add_argument("--legacy").default_value(false).implicit_value(true);
    program.add_argument("--n_threads").default_value(0)
        .action([](const std::string& value) { return std::stoi(value); });

    try {
        program.parse_args(argc, argv);
    }
    catch (const runtime_error& err) {
        cout << err.what() << endl;
        cout << program;
        exit(1);
    }

    return program;

}

int main (int argc, char* argv[]) {

    auto args = parse_args(argc, argv);
    int verbosity = args.get<int>("--verbosity");

    bool legacy_mode = args.get<bool>("--legacy");

    string input_filename, yuv_output_filename, option_output_filename, info_output_filename;
    bool has_output = false;

    input_filename = args.get<string>("h264_video");
    if(!filesystem::exists(input_filename)){
        cout << "input file does not exist: " << input_filename << endl;
        exit(1);
    }
    if(args.is_used("--yuv_out")) {
        yuv_output_filename = args.get<string>("--yuv_out");
        has_output = true;
    }
    if(args.is_used("--info_out")) {
        info_output_filename = args.get<string>("--info_out");
        has_output = true;
    }
    if(args.is_used("--option_out")) {
        option_output_filename = args.get<string>("--option_out");
        has_output = true;
    }

    if(!has_output) {
        cout << "no output modality is specified, program exiting" << endl;
        exit(0);
    }

    ISVCDecoder* pDecoder = NULL;
    SDecodingParam sDecParam = {0}; // all pointers initialized with NULL
    sDecParam.sVideoProperty.size = sizeof (sDecParam.sVideoProperty);
    //sDecParam.eEcActiveIdc = ERROR_CON_SLICE_MV_COPY_CROSS_IDR_FREEZE_RES_CHANGE;

    sDecParam.uiTargetDqLayer = (uint8_t) - 1;
    sDecParam.eEcActiveIdc = ERROR_CON_SLICE_COPY;
    sDecParam.sVideoProperty.eVideoBsType = VIDEO_BITSTREAM_DEFAULT;

    bool has_info_writer = false;
    unique_ptr<viper_purdue::ConcurrentWriter> info_writer;
    if(args.is_used("--info_out")){
        info_writer = make_unique<viper_purdue::ConcurrentWriter>(args.get<string>("--info_out"));
        has_info_writer = true;
        sDecParam.pExtraInfoWriter = info_writer.get();
    }

    if (WelsCreateDecoder (&pDecoder)  || (NULL == pDecoder)) {
        printf ("Create Decoder failed.\n");
        return 1;
    }

    pDecoder->SetOption (DECODER_OPTION_TRACE_LEVEL, &verbosity);
    int32_t thread_count = args.get<int>("--n_threads");
    pDecoder->SetOption (DECODER_OPTION_NUM_OF_THREADS, &thread_count);

    if (pDecoder->Initialize (&sDecParam)) {
        printf ("Decoder initialization failed.\n");
        return 1;
    }

    int32_t width = 0;
    int32_t height = 0;

    json option_output;
    option_output["h264_filename"] = input_filename;
    json *option_output_ptr = nullptr;
    if(args.is_used("--option_out")) {
        option_output_ptr = &option_output;
    }

    H264DecodeInstance (pDecoder, input_filename.c_str(),
                        !yuv_output_filename.empty() ? yuv_output_filename.c_str() : NULL,
                        width, height,
                        option_output_ptr, NULL,
                        (int32_t)sDecParam.eEcActiveIdc,
                        legacy_mode);

    if (sDecParam.pFileNameRestructed != NULL) {
        delete []sDecParam.pFileNameRestructed;
        sDecParam.pFileNameRestructed = NULL;
    }

    if (pDecoder) {
        pDecoder->Uninitialize();
        WelsDestroyDecoder (pDecoder);
    }

    // decoding has finished, terminate info writer
    if(has_info_writer) {
        info_writer->terminate();
    }

    if(args.is_used("--option_out")){
        fstream outfile{option_output_filename, ios::out};
        outfile << setw(2) << option_output;
    }

    return 0;
}
