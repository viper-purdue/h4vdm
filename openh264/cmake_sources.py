import json
import textwrap
import os

with open('cmake_sources.json', 'r') as infile:
    src = json.load(infile)

def cmake_add_source(obj_type, obj_name, files, shared_lib=False):
    files_str = [' ' * 4 + x  for x in files]
    if obj_type == 'library':
        obj_str = 'add_library'
        if shared_lib:
            obj_name += ' SHARED'
        else:
            obj_name += ' STATIC'
    elif obj_type == 'binary':
        obj_str = 'add_executable'
    return '{}({}\n{})'.format(obj_str, obj_name, '\n'.join(files_str))


static_libs = ['common', 'console_common']
libraries = []
binaries = []

# print('#'*10)
# print('# this section is automatically generated with cmake_sources.py')
# for name, item in src.items():
#     file_list = item['cpp'] + item['asm']
#     base_dir = '${CMAKE_CURRENT_SOURCE_DIR}/' + item['directory']
#     fullname_file_list = [os.path.join(base_dir, x) for x in file_list]
#     if item['type'] == 'library':
#         shared_libs = not name in static_libs
#         print(cmake_add_source(item['type'], name, fullname_file_list, shared_libs))
#         libraries.append(name)
#     if item['type'] == 'binary':
#         print(cmake_add_source(item['type'], name, fullname_file_list))
#         binaries.append(name)
# print('set(openh264_bins {})'.format(' '.join(binaries)))
# print('set(openh264_libs {})'.format(' '.join(libraries)))
# for bin_name in binaries:
#     print('target_link_libraries({} ${{openh264_libs}})'.format(bin_name))
# print('#'*10)

print('#'*10)
print('# this section is automatically generated with cmake_sources.py')
for name, item in src.items():
    file_list = item['cpp']
    base_dir = '${CMAKE_CURRENT_SOURCE_DIR}/' + item['directory']
    fullname_file_list = [os.path.join(base_dir, x) for x in file_list]
    if item['type'] == 'library':
        #shared_libs = not name in static_libs
        shared_libs = False
        print(cmake_add_source(item['type'], name, fullname_file_list, shared_libs))
        libraries.append(name)
        if len(item.get('asm', [])) > 0:
            fullname_asm_file_list = [os.path.join(base_dir, x) for x in item['asm']]
            print(cmake_add_source(item['type'], name + '_asm', fullname_asm_file_list, shared_libs))
            libraries.append(name + '_asm')
    if item['type'] == 'binary':
        print(cmake_add_source(item['type'], name, fullname_file_list))
        binaries.append(name)
print('set(openh264_bins {})'.format(' '.join(binaries)))
print('set(openh264_libs {})'.format(' '.join(libraries)))
for bin_name in binaries:
    print('target_link_libraries({} ${{openh264_libs}})'.format(bin_name))
print('#'*10)