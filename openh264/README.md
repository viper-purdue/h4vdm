# Modified OpenH264 for Metadata Extraction

This build configuration only works with Linux on x86/x86_64 architecture.

## Notes

- The extra information will only be correctly shipped out when multi-threaded decoding is disabled.


## Development steps

### Generating `CMakeLists.txt`

- From the project root directory, run `build/mktargets_cmake.sh`. 
  The script will generate `cmake_sources.json` in the project root directory.
- Run `cmake_sources.py` to generate sources section for CMake. Replace the 
  corresponding in `CMakeLists.txt` with the output.
  
### Compiling `protobuf` headers
- Run `info_shipout/make_proto_cpp.sh` and `info_shipout/make_proto_python.sh`.